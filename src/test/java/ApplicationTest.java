import order.OrderRepository;
import order.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

class ApplicationTest {
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        ConnectionCreator connectionCreator = new ConnectionCreator();
        DataSource dataSource = connectionCreator.getDataSource();
        OrderRepository orderRepository = new OrderRepository(dataSource);
        orderService = new OrderService(orderRepository);
    }

    @Test
    void selectCustomersAndTotalSpentMoney() {
        orderService.selectCustomersAndTotalSpentMoney();
    }

    @Test
    void getMostPopularProduct() {
        orderService.getMostPopularProduct();
    }
}