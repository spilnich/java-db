CREATE TABLE orders
(
    id             SERIAL       NOT NULL
        CONSTRAINT orders_pk PRIMARY KEY,
    customer_id    INT          NOT NULL,
    delivery_place varchar(255) NOT NULL
);