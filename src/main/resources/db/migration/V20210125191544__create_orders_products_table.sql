CREATE TABLE ordersProducts
(
    order_id   INT NOT NULL
        CONSTRAINT ordersProducts_orders_id_fk REFERENCES orders (id),
    product_id INT NOT NULL
        CONSTRAINT ordersProducts_product_id_fk REFERENCES product (id),
    CONSTRAINT ordersProducts_pk PRIMARY KEY (order_id, product_id)
);