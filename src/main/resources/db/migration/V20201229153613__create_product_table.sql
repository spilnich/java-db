CREATE TABLE product
(
    id          INT              NOT NULL
        CONSTRAINT product_pk PRIMARY KEY,
    name        VARCHAR(255)     NOT NULL,
    description VARCHAR(255)     NOT NULL,
    price       DOUBLE PRECISION NOT NULL
);