CREATE TABLE customer
(
    id          INT          NOT NULL
        CONSTRAINT customer_pk PRIMARY KEY,
    firstname VARCHAR(50) NOT NULL,
    lastname  VARCHAR(50) NOT NULL,
    cellphone  VARCHAR(20)  NOT NULL
);