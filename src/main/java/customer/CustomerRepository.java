package customer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerRepository {

    public static final String EXCEPTION = " exception";
    private final DataSource dataSource;

    public CustomerRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Optional<Customer> insert(Customer customer) {
        String sql = "INSERT INTO customer (id, firstname, lastname, cellphone) VALUES (?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, customer.getId());
            ps.setString(2, customer.getFirstname());
            ps.setString(3, customer.getLastname());
            ps.setString(4, customer.getCellphone());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("insert customer: " + customer.getId() + EXCEPTION);
            return Optional.empty();
        }
        return Optional.of(customer);
    }

    public void update(Customer customer) {
        String sql = "UPDATE customer SET firstname=?, lastname=?, cellphone=? WHERE id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, customer.getFirstname());
            ps.setString(2, customer.getLastname());
            ps.setString(3, customer.getCellphone());
            ps.setInt(4, customer.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("update customer: " + customer.getId() + EXCEPTION);
        }
    }

    public void delete(Customer customer) {
        String sql = "DELETE FROM customer WHERE id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, customer.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("delete customer: " + customer.getId() + EXCEPTION);
        }
    }

    public List<Customer> getCustomers() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                customers.add(toCustomer(resultSet));
            }
        } catch (SQLException e) {
            System.out.println("get list of customers exception");
        }
        return customers;
    }

    private Customer toCustomer(ResultSet rs) throws SQLException {
        return new Customer(
                rs.getInt("id"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                rs.getString("cellphone")
        );
    }
}
