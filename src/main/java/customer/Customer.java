package customer;

public class Customer {

    private final int id;
    private final String firstname;
    private final String lastname;
    private final String cellphone;

    public Customer(int id, String firstname, String lastname, String cellphone) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.cellphone = cellphone;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCellphone() {
        return cellphone;
    }
}
