package order;

public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void insert(Order order) {
        orderRepository.insert(order);
    }

    public void update(Order order) {
        orderRepository.update(order);
    }

    public void delete(Order order) {
        orderRepository.delete(order);
    }

    public void insertIntoOrdersProducts(int orderId, int productId) {
        orderRepository.insertIntoOrdersProducts(orderId, productId);
    }

    public void selectCustomersAndTotalSpentMoney() {
        orderRepository.selectCustomersAndTotalSpentMoney();
    }

    public void getMostPopularProduct() {
        orderRepository.getMostPopularProduct();
    }

    public int totalOrderPrice(int customerId) {
        return orderRepository.totalOrderPrice(customerId);
    }

    public int orderProductQuantity(int customerId) {
        return orderRepository.orderProductQuantity(customerId);
    }
}
