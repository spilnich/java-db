package order;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderRepository {

    public static final String EXCEPTION = " exception";
    private final DataSource dataSource;

    public OrderRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void insert(Order order) {
        String sql = "INSERT INTO orders (customer_id, delivery_place) VALUES (?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, order.getCustomer().getId());
            ps.setString(2, order.getDeliveryPlace());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("insert order for customer: " + order.getCustomer().getId() + EXCEPTION);
        }
    }

    public void update(Order order) {
        String sql = "UPDATE orders SET delivery_place=? WHERE customer_id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, order.getDeliveryPlace());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("update order exception");
        }
    }

    public void delete(Order order) {
        String sql = "DELETE FROM orders WHERE customer_id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setObject(1, order.getCustomer());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("delete order for customer: " + order.getCustomer().getId() + EXCEPTION);
        }
    }

    public void insertIntoOrdersProducts(int orderId, int productId) {
        String sql = "INSERT INTO ordersproducts(order_id, product_id) VALUES (?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, orderId);
            ps.setInt(2, productId);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("initSave ides exception");
        }
    }

    public void selectCustomersAndTotalSpentMoney() {
        String sql = "SELECT c.firstname AS \"CUSTOMER\", SUM(p.price) AS \"TOTAL SPENT\" " +
                "FROM customer c LEFT JOIN orders o ON c.id = o.customer_id " +
                "LEFT JOIN ordersproducts o2 on o.id = o2.order_id LEFT JOIN product p on p.id = o2.product_id " +
                "GROUP BY c.firstname";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet resultSet = ps.executeQuery();

            System.out.println("CUSTOMER" + "      " + "TOTAL SPENT");
            System.out.println("=====================================");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("CUSTOMER") + "      "
                        + resultSet.getDouble("TOTAL SPENT"));
            }
        } catch (SQLException e) {
            System.out.println("select customers total spent money exception");
        }
    }

    void getMostPopularProduct() {
        String sql = "SELECT p.name AS \"MOST POPULAR PRODUCT\", COUNT(p.name) AS \"PURCHASES\" FROM orders ord " +
                "INNER JOIN ordersproducts o on ord.id = o.order_id LEFT JOIN product p on p.id = o.product_id " +
                "GROUP BY p.name ORDER BY \"PURCHASES\" DESC LIMIT 1";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            String popularProductName = resultSet.getString("MOST POPULAR PRODUCT");
            int productPurchases = resultSet.getInt("PURCHASES");

            System.out.println();
            System.out.println("THE MOST POPULAR PRODUCT:       PURCHASES:" + "\n" +
                    popularProductName + "                              " + productPurchases + "\n");
        } catch (SQLException e) {
            System.out.println("select most popular product exception");
        }
    }

    public int totalOrderPrice(int customerId) {
        String sql = "SELECT c.id, SUM(p.price) FROM orders ord INNER JOIN customer c ON c.id = ord.id " +
                "LEFT JOIN ordersproducts o on ord.id = o.order_id LEFT JOIN product p on p.id = o.product_id " +
                "GROUP BY c.id HAVING c.id = ?";

        int totalPrice = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            totalPrice = resultSet.getInt(2);
        } catch (SQLException e) {
            System.out.println("select total order price for customer: " + customerId + EXCEPTION);
        }
        return totalPrice;
    }

    public int orderProductQuantity(int customerId) {
        String sql = "SELECT c.id, COUNT(p.name) FROM orders ord INNER JOIN customer c ON c.id = ord.id " +
                "LEFT JOIN ordersproducts o on ord.id = o.order_id LEFT JOIN product p on p.id = o.product_id " +
                "GROUP BY c.id HAVING c.id = ?";

        int quantity = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            quantity = resultSet.getInt(2);
        } catch (SQLException e) {
            System.out.println("select order products quantity for customer: " + customerId + EXCEPTION);
        }
        return quantity;
    }
}
