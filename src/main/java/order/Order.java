package order;

import customer.Customer;

public class Order {

    private final Customer customer;
    private final String deliveryPlace;

    public Order(Customer customer, String deliveryPlace) {
        this.customer = customer;
        this.deliveryPlace = deliveryPlace;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getDeliveryPlace() {
        return deliveryPlace;
    }
}
