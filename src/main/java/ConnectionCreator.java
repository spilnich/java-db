import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectionCreator {
    private final DataSource dataSource;

    public ConnectionCreator() {
        this.dataSource = createDatasource();
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public Flyway createFlyway(DataSource dataSource) {
        return Flyway.configure()
                .dataSource(dataSource)
                .load();
    }

    public DataSource createDatasource() {
        Properties properties = getProperties();

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(properties.getProperty("jdbc.url"));
        hikariConfig.setUsername(properties.getProperty("jdbc.username"));
        hikariConfig.setPassword(properties.getProperty("jdbc.password"));
        hikariConfig.setMaximumPoolSize(Integer.parseInt(properties.getProperty("jdbc.pool.maxConnection")));

        return new HikariDataSource(hikariConfig);
    }

    private Properties getProperties() {
        Properties properties = new Properties();

        InputStream inputStream = Application.class.getClassLoader().getResourceAsStream("application.properties");

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            System.out.println("Exception while loading properties");
        }
        return properties;
    }
}
