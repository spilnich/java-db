package product;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductRepository {

    public static final String EXCEPTION = " exception";
    private final DataSource dataSource;

    public ProductRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Optional<Product> insert(Product product) {
        String sql = "INSERT INTO product (id, name, description, price) VALUES (?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, product.getId());
            ps.setString(2, product.getName());
            ps.setString(3, product.getDescription());
            ps.setDouble(4, product.getCurrentPrice());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("insert product: " + product.getId() + EXCEPTION);
            return Optional.empty();
        }
        return Optional.of(product);
    }

    public void update(Product product) {
        String sql = "UPDATE product SET name=?, description=?, price=? WHERE id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, product.getName());
            ps.setString(2, product.getDescription());
            ps.setDouble(3, product.getCurrentPrice());
            ps.setInt(4, product.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("update product: " + product.getId() + EXCEPTION);
        }
    }

    public void delete(Product product) {
        String sql = "DELETE FROM product WHERE id=?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, product.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("delete product: " + product.getId() + EXCEPTION);
        }
    }

    public List<Product> getOrderProducts(int customerId) {
        String sql = "SELECT p.name FROM orders ord INNER JOIN customer c ON c.id = ord.id " +
                "LEFT JOIN ordersproducts o on ord.id = o.order_id LEFT JOIN product p on p.id = o.product_id " +
                " WHERE c.id = ?";

        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, customerId);

            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                products.add(toProduct(resultSet));
            }
        } catch (SQLException e) {
            System.out.println("get list of products for customer: " + customerId + EXCEPTION);
        }
        return products;
    }

    private Product toProduct(ResultSet rs) throws SQLException {
        return new Product(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getDouble("price")
        );
    }
}
