package product;

public class Product {

    private final int id;
    private final String name;
    private final String description;
    private final double currentPrice;

    public Product(int id, String name, String description, double currentPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.currentPrice = currentPrice;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    @Override
    public String toString() {
        return name;
    }
}
