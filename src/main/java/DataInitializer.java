import customer.Customer;
import customer.CustomerService;
import order.Order;
import order.OrderService;
import product.Product;
import product.ProductService;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class DataInitializer {
    private final CustomerService customerService;
    private final ProductService productService;
    private final OrderService orderService;

    public DataInitializer(CustomerService customerService, ProductService productService, OrderService orderService) {
        this.customerService = customerService;
        this.productService = productService;
        this.orderService = orderService;
    }

    private void fillCustomers() {
        IntStream.rangeClosed(1, 100).forEach(i -> customerService.insert(new Customer(
                i,
                "firstname" + i,
                "lastname" + i,
                "+380" + ThreadLocalRandom.current().nextInt(100000000, 999999999)
        )));
    }

    private void fillProducts() {
        IntStream.rangeClosed(1, 100).forEach(i -> productService.insert(new Product(
                i,
                "name" + i,
                "description" + i,
                ThreadLocalRandom.current().nextDouble(10, 1000)
        )));
    }

    private void fillOrders() {
        IntStream.rangeClosed(1, 50).forEach(i -> orderService.insert(new Order(
                customerService.getCustomers().get(ThreadLocalRandom.current().nextInt(1, 100)),
                "delivery_place" + i
        )));
    }

    private void fillOrdersProducts() {
        IntStream.rangeClosed(1, 100).forEach(i -> orderService.insertIntoOrdersProducts(
                ThreadLocalRandom.current().nextInt(1, 50),
                ThreadLocalRandom.current().nextInt(1, 100)
        ));
        IntStream.rangeClosed(1, 50).forEach(i -> orderService.insertIntoOrdersProducts(
                i,
                ThreadLocalRandom.current().nextInt(1, 100)
        ));
    }

    public void initialize() {
        fillCustomers();
        fillOrders();
        fillProducts();
        fillOrdersProducts();
    }
}
