import customer.CustomerRepository;
import customer.CustomerService;
import order.OrderRepository;
import order.OrderService;
import org.flywaydb.core.Flyway;
import product.ProductRepository;
import product.ProductService;

import javax.sql.DataSource;

public class Application {

    public static void main(String[] args) throws InterruptedException {

        ConnectionCreator connectionCreator = new ConnectionCreator();
        DataSource dataSource = connectionCreator.getDataSource();

        CustomerService customerService = new CustomerService(new CustomerRepository(dataSource));
        ProductService productService = new ProductService(new ProductRepository(dataSource));
        OrderService orderService = new OrderService(new OrderRepository(dataSource));

        Flyway flyway = connectionCreator.createFlyway(dataSource);
        flyway.migrate();
        Thread.sleep(1000);

        DataInitializer dataInitializer = new DataInitializer(customerService, productService, orderService);
        dataInitializer.initialize();
    }
}
